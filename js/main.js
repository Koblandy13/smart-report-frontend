'use strict'
let selectedType = 1,
    SHOW_TIME = 1000,
    HIDE_TIME = 1000,
    DELAY_TIME = 1000
$(document).ready(() => {
    $.get("http://api.kgp.kz:9406//SmartReport/sr2/parent_districts")
        .done(data => {
            data.list.forEach(region => {
                if (region.parent_district_id !== '0')
                    $('#regions').append(`<option value="${region.parent_district_id}">${region.parent_district_desc}</option>`)
            })
        })
    $('#download-report').click(() => {
        let selector = ''
        switch (selectedType) {
            case 1:
                selector = $('#first-block tbody tr td .type-one-checkbox')
                break
            case 2:
                selector = $('#second-block tbody tr td .type-one-checkbox')
                break
            case 3:
                selector = $('#third-block tbody tr td .type-one-checkbox')
                break
            case 4:
                selector = $('#fourth-block tbody tr td .type-one-checkbox')
                break

        }
        let reportType = ''
        $.each(selector, (index, checkbox) => {
            if (checkbox.checked) {
                reportType += checkbox.value + ","
            }
        })
        reportType = reportType.substring(0, reportType.length - 1)
        let month = new Date('2018', $('#months').val(), 0)
        if (month.getMonth() + 1 < 10) month = `${month.getDate()}.0${month.getMonth() + 1}.18`
        else month = `${month.getDate()}.${month.getMonth() + 1}.${month.getFullYear()}`
        if ($('#regions').val() && reportType.length > 0 && $('#months').val()) {
            let crime_region = $('#regions').val()
            location.href = `http://api.kgp.kz:3000/download?type=${reportType}&crime_region=${crime_region}&month=${month}`
        } else {
            alert('Выберите месяц, тип отчета и регион')
        }
    })

    $('.block-type').change(() => {
        let blockType = $('.block-type').val()
        switch (+blockType) {
            case 1:
                setTimeout(()=> {
                    getBlockSelector(1).show(SHOW_TIME)
                },DELAY_TIME)

                getBlockSelector(2).hide(HIDE_TIME)
                getBlockSelector(3).hide(HIDE_TIME)
                getBlockSelector(4).hide(HIDE_TIME)
                setSelectedType(+blockType)
                break
            case 2:
                setTimeout(()=> {
                    getBlockSelector(2).show(SHOW_TIME)
                },DELAY_TIME)
                getBlockSelector(1).hide(HIDE_TIME)
                getBlockSelector(3).hide(HIDE_TIME)
                getBlockSelector(4).hide(HIDE_TIME)
                setSelectedType(+blockType)
                break
            case 3:
                setTimeout(()=> {
                    getBlockSelector(3).show(SHOW_TIME)
                },DELAY_TIME)
                getBlockSelector(1).hide(HIDE_TIME)
                getBlockSelector(2).hide(HIDE_TIME)
                getBlockSelector(4).hide(HIDE_TIME)
                setSelectedType(+blockType)
                break
            case 4:
                setTimeout(()=> {
                    getBlockSelector(4).show(SHOW_TIME)
                },DELAY_TIME)
                getBlockSelector(1).hide(HIDE_TIME)
                getBlockSelector(2).hide(HIDE_TIME)
                getBlockSelector(3).hide(HIDE_TIME)
                setSelectedType(+blockType)
                break
        }
    })
})
let getBlockSelector = (type) => {
    switch (type) {
        case 1:
            return $('#first-block')
        case 2:
            return $('#second-block')
        case 3:
            return $('#third-block')
        case 4:
            return $('#fourth-block')
    }
}

let setSelectedType = (type) => {
    selectedType = type
}